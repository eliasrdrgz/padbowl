package com.jhipster.padbowl.game.infrastructure.secondary;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.PadBowlIntTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@PadBowlIntTest
class GamesSpringRepositoryIntTest {
  @Autowired
  private GamesSpringRepository repository;

  @Test
  void shouldSaveAndGetWithRepository() {
    repository.save(entity());

    assertThat(repository.findById(id()).get()).usingRecursiveComparison().isEqualTo(entity());
  }

  private GameEntity entity() {
    return GameEntity.from(threeRolls());
  }
}
