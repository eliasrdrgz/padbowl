package com.jhipster.padbowl.game.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.cucumber.CucumberTestContext;
import com.jhipster.padbowl.cucumber.MockedSecurityContextRepository;
import com.jhipster.padbowl.security.AuthoritiesConstants;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

public class GamesSteps {
  private static final Map<String, Authentication> PLAYERS = Map.of(
    "Player1",
    new TestingAuthenticationToken("Player1", "N/A", AuthorityUtils.createAuthorityList(AuthoritiesConstants.PLAYER))
  );

  @Autowired
  private MockedSecurityContextRepository contexts;

  @Autowired
  private TestRestTemplate rest;

  @Given("{string} creates a game")
  public void playerCreatesAGame(String playerName) {
    Authentication authentication = PLAYERS.get(playerName);
    contexts.authentication(authentication);
    SecurityContextHolder.getContext().setAuthentication(authentication);

    ResponseEntity<Void> response = rest.postForEntity("/api/games", null, Void.class);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @When("Player roll")
  public void roll(List<Integer> rolls) {
    String gameId = currentGameId();

    rolls.forEach(
      roll -> {
        rest.postForEntity("/api/games/" + gameId + "/rolls", new RestRoll(roll), Void.class);
      }
    );
  }

  @Then("The roll is invalid")
  public void shouldHaveInvalidRoll() {
    assertThat(CucumberTestContext.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Then("The score is {int}")
  public void shouldHaveScore(int score) {
    int gameScore = (int) CucumberTestContext.getElement("$.score");

    assertThat(gameScore).isEqualTo(score);
  }

  @Then("The game player name is {string}")
  public void shouldHavePlayer(String playerName) {
    String player = (String) CucumberTestContext.getElement("rolls", "$.player");

    assertThat(player).isEqualTo(playerName);
  }

  @Then("The game frames are")
  public void shouldHaveFrames(List<Map<String, Integer>> frames) {
    for (int frame = 0; frame < frames.size(); frame++) {
      String framePath = "$.frames[" + frame + "].";

      assertThat(CucumberTestContext.getElement(framePath + "firstRoll")).isEqualTo(frames.get(frame).get("First roll"));
      assertThat(CucumberTestContext.getElement(framePath + "secondRoll")).isEqualTo(frames.get(frame).get("Second roll"));
    }
  }

  private String currentGameId() {
    return (String) CucumberTestContext.getElement("games", "$.id");
  }

  @Then("{string} is shamed")
  public void shouldShamePlayer(String playerName) throws IOException {
    assertThat(Files.readString(Paths.get("target/shame.txt"))).isEqualTo(playerName);
  }
}
