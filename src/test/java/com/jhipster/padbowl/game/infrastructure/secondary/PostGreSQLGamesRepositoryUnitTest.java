package com.jhipster.padbowl.game.infrastructure.secondary;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.jhipster.padbowl.common.domain.error.MissingMandatoryValueException;
import com.jhipster.padbowl.game.domain.Game;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PostGreSQLGamesRepositoryUnitTest {
  @Mock
  private GamesSpringRepository springRepository;

  @InjectMocks
  private PostGreSQLGamesRepository repository;

  @Test
  void shouldNotSaveWithoutGame() {
    assertThatThrownBy(() -> repository.save(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("game");
  }

  @Test
  void shouldSaveGameUsingSpringRepository() {
    Game game = threeRolls();

    repository.save(game);

    verify(springRepository).save(notNull());
  }

  @Test
  void shouldNotGetGameWithoutId() {
    assertThatThrownBy(() -> repository.get(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("id");
  }

  @Test
  void shouldGetGameFromRepository() {
    when(springRepository.findById(id())).thenReturn(Optional.of(GameEntity.from(threeRolls())));

    assertThat(repository.get(gameId()).get()).usingRecursiveComparison().isEqualTo(threeRolls());
  }
}
