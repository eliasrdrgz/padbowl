package com.jhipster.padbowl.game.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import org.junit.jupiter.api.Test;

class RestRollUnitTest {

  @Test
  void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(roll())).isEqualTo(json());
  }

  @Test
  void shouldDeserializeFromJson() {
    assertThat(TestJson.readFromJson(json(), RestRoll.class)).usingRecursiveComparison().isEqualTo(roll());
  }

  private RestRoll roll() {
    return new RestRoll(2);
  }

  private String json() {
    return "{\"roll\":2}";
  }
}
