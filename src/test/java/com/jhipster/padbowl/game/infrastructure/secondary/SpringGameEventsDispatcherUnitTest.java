package com.jhipster.padbowl.game.infrastructure.secondary;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.jhipster.padbowl.common.domain.Guttered;
import com.jhipster.padbowl.common.domain.error.MissingMandatoryValueException;
import com.jhipster.padbowl.game.domain.GamesFixture;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SpringGameEventsDispatcherUnitTest {
  @Mock
  private ApplicationEventPublisher publisher;

  @InjectMocks
  private SpringGameEventsDispatcher dispatcher;

  @Test
  void shouldNotSendWithoutEvents() {
    assertThatThrownBy(() -> dispatcher.dispatch(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("events");
  }

  @Test
  void shouldDispatchUsingPublisher() {
    Guttered event = new Guttered(GamesFixture.playerName());
    dispatcher.dispatch(List.of(event));

    verify(publisher).publishEvent(event);
  }
}
