package com.jhipster.padbowl.game.domain;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.Guttered;
import java.util.Collection;
import org.junit.jupiter.api.Test;

class GameUnitTest {
  private final Game game = new Game("Player");

  @Test
  void shouldGetNewGameInformation() {
    assertThat(game.getScore()).isEqualTo(0);
    assertThat(game.getFrames()).isEmpty();
    assertThat(game.getPlayer()).isEqualTo(new PlayerName("Player"));
    assertThat(game.getId()).isNotNull();
  }

  @Test
  void shouldGetExistingGame() {
    Game game = new Game(id(), "Player2");

    assertThat(game.getId()).isEqualTo(gameId());
    assertThat(game.getScore()).isEqualTo(0);
    assertThat(game.getFrames()).isEmpty();
    assertThat(game.getPlayer()).isEqualTo(new PlayerName("Player2"));
  }

  @Test
  void shouldScoreGutterGame() {
    rolls(20, 0);

    assertThat(game.getScore()).isEqualTo(0);
  }

  @Test
  void shouldScoreGameOfOnes() {
    rolls(20, 1);

    assertThat(game.getScore()).isEqualTo(20);
  }

  @Test
  void shouldScoreSpare() {
    game.roll(6);
    game.roll(4);
    game.roll(2);

    assertThat(game.getScore()).isEqualTo(10 + 2 + 2);
  }

  @Test
  void shouldScoreStrike() {
    game.roll(10);
    game.roll(2);
    game.roll(3);

    assertThat(game.getScore()).isEqualTo(10 + (2 + 3) + (2 + 3));
  }

  @Test
  void shouldScorePerfectGame() {
    rolls(12, 10);

    assertThat(game.getScore()).isEqualTo(300);
  }

  @Test
  void shouldEndWithStrike() {
    rolls(18, 4);

    game.roll(10);
    game.roll(2);
    game.roll(3);

    assertThat(game.getScore()).isEqualTo(4 * 18 + 10 + (2 + 3));
  }

  @Test
  void shouldGetFramesForOneRollGame() {
    game.roll(2);

    assertThat(game.getFrames()).usingRecursiveFieldByFieldElementComparator().containsExactly(new Frame(2));
  }

  @Test
  void shouldGetFramesForThreeRollsGame() {
    game.roll(2);
    game.roll(4);
    game.roll(3);

    assertThat(game.getFrames()).usingRecursiveFieldByFieldElementComparator().containsExactly(new Frame(2).secondRoll(4), new Frame(3));
  }

  @Test
  void shouldGetFramesWithStartingStrike() {
    game.roll(10);
    game.roll(4);
    game.roll(3);

    assertThat(game.getFrames()).usingRecursiveFieldByFieldElementComparator().containsExactly(new Frame(10), new Frame(4).secondRoll(3));
  }

  @Test
  void shouldGetUnmodifiableFrames() {
    assertThatThrownBy(() -> game.getFrames().clear()).isExactlyInstanceOf(UnsupportedOperationException.class);
  }

  @Test
  void shouldHaveGutteredEventForGutter() {
    Collection<Guttered> events = game.roll(0);

    assertThat(events).usingRecursiveFieldByFieldElementComparator().containsExactly(new Guttered(GamesFixture.playerName()));
  }

  @Test
  void shouldNotHaveGutteredEventForCorrectRoll() {
    assertThat(game.roll(3)).isEmpty();
  }

  private void rolls(int rolls, int pinsDown) {
    for (int roll = 0; roll < rolls; roll++) {
      game.roll(pinsDown);
    }
  }
}
