package com.jhipster.padbowl.common.infrastructure.primary.error;

public class NotDeserializable {
  private String value;

  public NotDeserializable(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
