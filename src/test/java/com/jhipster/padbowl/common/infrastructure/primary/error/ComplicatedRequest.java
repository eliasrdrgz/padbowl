package com.jhipster.padbowl.common.infrastructure.primary.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Pattern;

public class ComplicatedRequest {
  private String value;

  public ComplicatedRequest(@JsonProperty("value") String value) {
    this.value = value;
  }

  @Pattern(message = ValidationMessage.WRONG_FORMAT, regexp = "complicated")
  public String getValue() {
    return value;
  }
}
