package com.jhipster.padbowl.common.infrastructure.primary.error;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.error.PadBowlMessage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

public class ErrorMessagesUnitTest {
  private static final Set<Class<? extends PadBowlMessage>> errors = new Reflections(
    new ConfigurationBuilder()
      .setUrls(ClasspathHelper.forPackage("com.jhipster"))
      .setScanners(new SubTypesScanner())
      .filterInputsBy(new FilterBuilder().includePackage("com.jhipster"))
  )
  .getSubTypesOf(PadBowlMessage.class);

  @Test
  public void shouldHaveOnlyEnumImplementations() {
    errors.forEach(
      error ->
        assertThat(error.isEnum() || error.isInterface())
          .as("Implementations of " + PadBowlMessage.class.getName() + " must be enums and " + error.getName() + " wasn't")
          .isTrue()
    );
  }

  @Test
  public void shouldHaveMessagesForAllKeys() {
    Collection<Properties> messages = loadMessages();

    errors
      .stream()
      .filter(error -> error.isEnum())
      .forEach(error -> Arrays.stream(error.getEnumConstants()).forEach(value -> messages.forEach(assertMessageExist(value))));
  }

  private List<Properties> loadMessages() {
    try {
      return Files.list(Paths.get("src/main/resources/i18n")).map(toProperties()).collect(Collectors.toList());
    } catch (IOException e) {
      throw new AssertionError();
    }
  }

  private Function<Path, Properties> toProperties() {
    return file -> {
      Properties properties = new Properties();
      try (InputStream input = Files.newInputStream(file)) {
        properties.load(input);
      } catch (IOException e) {
        throw new AssertionError();
      }

      return properties;
    };
  }

  private Consumer<Properties> assertMessageExist(PadBowlMessage value) {
    return currentMessages -> {
      assertThat(currentMessages.getProperty("padbowl.error." + value.getMessageKey()))
        .as("Can't find message for " + value.getMessageKey() + " in all files, check your configuration")
        .isNotBlank();
    };
  }
}
