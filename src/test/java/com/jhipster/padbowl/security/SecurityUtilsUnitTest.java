package com.jhipster.padbowl.security;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.jhipster.padbowl.common.infrastructure.primary.error.NotAuthenticatedUserException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Test class for the {@link SecurityUtils} utility class.
 */
public class SecurityUtilsUnitTest {

  @BeforeEach
  void resetAuthentication() {
    SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
  }

  @Test
  void shouldNotGetCurrentUserLoginForNotAuthenticatedUser() {
    assertThatThrownBy(() -> SecurityUtils.getCurrentUserLogin()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
  }

  @Test
  void shouldNotGetCurrentUserLoginForDummyAuthenticatedUser() {
    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(mock(Authentication.class));
    SecurityContextHolder.setContext(securityContext);

    assertThatThrownBy(() -> SecurityUtils.getCurrentUserLogin()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
  }

  @Test
  void shouldGetAuthenticatedUserFromUserDetails() {
    UserDetails details = mock(UserDetails.class);
    when(details.getUsername()).thenReturn("padbowl");

    Authentication authentication = mock(Authentication.class);
    when(authentication.getPrincipal()).thenReturn(details);

    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);

    assertThat(SecurityUtils.getCurrentUserLogin()).isEqualTo("padbowl");
  }

  @Test
  void shouldGetAuthenticationFromString() {
    Authentication authentication = mock(Authentication.class);
    when(authentication.getPrincipal()).thenReturn("padbowl");

    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);

    assertThat(SecurityUtils.getCurrentUserLogin()).isEqualTo("padbowl");
  }

  @Test
  void shouldGetEmptyRolesForNotAuthenticatedUser() {
    assertThat(SecurityUtils.getAuthenticatedUserRoles()).isEmpty();
  }

  @Test
  void shouldGetRolesFromAuthorities() {
    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(
      new UsernamePasswordAuthenticationToken(
        "padbowl",
        "admin",
        List.of(new SimpleGrantedAuthority("ROLE_USER"), new SimpleGrantedAuthority("DUMMY"))
      )
    );
    SecurityContextHolder.setContext(securityContext);

    assertThat(SecurityUtils.getAuthenticatedUserRoles()).containsExactly("ROLE_USER");
  }

  public static void mockAuthenticatedOfferManager() {
    mockAuthenticatedUser("OM");
  }

  public static void mockAuthenticatedUser(String... roles) {
    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    List<SimpleGrantedAuthority> userRoles = Arrays
      .stream(roles)
      .map(role -> "ROLE_" + role)
      .map(SimpleGrantedAuthority::new)
      .collect(Collectors.toList());

    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("padbowl", "admin", userRoles));

    SecurityContextHolder.setContext(securityContext);
  }
}
