package com.jhipster.padbowl.kipe;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.PadBowlIntTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;

@PadBowlIntTest
class KipeConfigurationIntTest {
  @Autowired
  private SecuredTestService service;

  @Test
  void shouldNotBeAbleToMakeOperationWhenNotAuthenticated() {
    assertThatThrownBy(() -> service.cannotMakeAnUnknownOperation(new Dummy("pouet")))
      .isExactlyInstanceOf(AuthenticationCredentialsNotFoundException.class);
  }

  @Test
  @WithMockUser
  void shouldNotBeAbleToMakeAnUnknownDummyOperation() {
    assertThatThrownBy(() -> service.cannotMakeAnUnknownOperation(new Dummy("pouet"))).isExactlyInstanceOf(AccessDeniedException.class);
  }
}
