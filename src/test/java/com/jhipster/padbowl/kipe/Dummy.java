package com.jhipster.padbowl.kipe;

class Dummy {
  private final String value;

  public Dummy(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
