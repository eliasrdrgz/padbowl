package com.jhipster.padbowl.kipe;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.jhipster.padbowl.common.domain.Username;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class KipeMethodSecurityExpressionRootUnitTest {
  @Mock
  private Authentication authentication;

  @Mock
  private CanEvaluator evaluator;

  @InjectMocks
  private KipeMethodSecurityExpressionRoot expressionRoot;

  @Test
  void shouldEvaluateUsingCanEvaluator() {
    when(evaluator.can(authentication, "action", user())).thenReturn(true);

    assertThat(expressionRoot.can("action", user())).isTrue();
  }

  private Username user() {
    return new Username("user");
  }
}
