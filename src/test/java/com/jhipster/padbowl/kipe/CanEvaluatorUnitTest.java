package com.jhipster.padbowl.kipe;

import static org.assertj.core.api.Assertions.*;

import ch.qos.logback.classic.Level;
import com.jhipster.padbowl.common.LogSpy;
import com.jhipster.padbowl.common.domain.error.MissingMandatoryValueException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;

@ExtendWith(LogSpy.class)
class CanEvaluatorUnitTest {
  @Mock
  private Authentication authentication;

  private final LogSpy logs;

  public CanEvaluatorUnitTest(LogSpy logs) {
    this.logs = logs;
  }

  @Test
  void shouldNotBuildWithoutObjectChecker() {
    assertThatThrownBy(() -> new CanEvaluator(List.of())).isExactlyInstanceOf(MissingMandatoryValueException.class);
  }

  @Test
  void shouldResolveOnDefaultCheckerForNullObject() {
    CanEvaluator canEvaluator = new CanEvaluator(List.of(new ObjectChecker()));

    canEvaluator.can(authentication, "action", null);

    logs.assertLogged(Level.ERROR, "default handler");
  }

  @Test
  void shouldResolveOnDefaultCheckerForUnknownType() {
    CanEvaluator canEvaluator = new CanEvaluator(List.of(new ObjectChecker()));

    canEvaluator.can(authentication, "action", "yo");

    logs.assertLogged(Level.ERROR, "default handler");
  }

  @Test
  void shouldGetMatchingEvaluator() {
    CanEvaluator canEvaluator = new CanEvaluator(List.of(new ObjectChecker(), new DummyChecker()));

    assertThat(canEvaluator.can(authentication, "action", new Dummy("pouet"))).isTrue();
  }

  @Test
  void shouldGetMatchingEvaluatorForChildClass() {
    CanEvaluator canEvaluator = new CanEvaluator(List.of(new ObjectChecker(), new DummyChecker()));

    assertThat(canEvaluator.can(authentication, "action", new DummyChild("pouet"))).isTrue();
    assertThat(canEvaluator.can(authentication, "action", new DummyChild("pouet"))).isTrue();
    logs.assertLogged(Level.INFO, "evaluator", 1);
  }
}
