package com.jhipster.padbowl.kipe;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class DummyChecker implements CanChecker<Dummy> {

  @Override
  public boolean can(Authentication authentication, String action, Dummy item) {
    return "action".equals(action) && "pouet".equals(item.getValue());
  }
}
