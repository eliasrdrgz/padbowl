package com.jhipster.padbowl.common.domain.error;

public final class Assert {

  private Assert() {}

  public static void notNull(String field, Object input) {
    if (input == null) {
      throw MissingMandatoryValueException.forNullValue(field);
    }
  }

  public static void notBlank(String field, String input) {
    notNull(field, input);

    if (input.isBlank()) {
      throw MissingMandatoryValueException.forBlankValue(field);
    }
  }

  public static StringAsserter field(String fieldName, String value) {
    return new StringAsserter(fieldName, value);
  }

  public static class StringAsserter {
    private final String fieldName;
    private final String value;

    private StringAsserter(String fieldName, String value) {
      this.fieldName = fieldName;
      this.value = value;
    }

    public StringAsserter notBlank() {
      Assert.notBlank(fieldName, value);

      return this;
    }

    public StringAsserter maxLength(int maxLength) {
      if (value == null || value.length() <= maxLength) {
        return this;
      }

      throw StringTooLongException.builder().field(fieldName).currentLength(value.length()).maxLength(maxLength).build();
    }
  }
}
