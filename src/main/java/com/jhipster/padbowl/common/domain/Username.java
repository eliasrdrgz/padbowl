package com.jhipster.padbowl.common.domain;

import com.jhipster.padbowl.common.domain.error.Assert;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Username {
  private final String username;

  public Username(String username) {
    Assert.field("username", username).notBlank().maxLength(50);

    this.username = username;
  }

  public String get() {
    return username;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(username).build();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    Username other = (Username) obj;
    return username.equalsIgnoreCase(other.username);
  }
}
