package com.jhipster.padbowl.common.infrastructure.primary.error;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;

public class NotAuthenticatedUserException extends PadBowlException {

  public NotAuthenticatedUserException() {
    super(
      PadBowlException
        .builder(AuthenticationMessage.NOT_AUTHENTICATED)
        .status(ErrorStatus.UNAUTHORIZED)
        .message("User is not authenticated")
    );
  }
}
