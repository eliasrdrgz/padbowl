package com.jhipster.padbowl.common.infrastructure.primary.error;

import com.jhipster.padbowl.common.domain.error.PadBowlMessage;

enum AuthenticationMessage implements PadBowlMessage {
  NOT_AUTHENTICATED("user.authentication-not-authenticated");

  private final String messageKey;

  AuthenticationMessage(String messageKey) {
    this.messageKey = messageKey;
  }

  @Override
  public String getMessageKey() {
    return messageKey;
  }
}
