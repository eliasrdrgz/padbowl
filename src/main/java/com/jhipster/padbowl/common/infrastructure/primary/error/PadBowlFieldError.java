/**
 *
 */
package com.jhipster.padbowl.common.infrastructure.primary.error;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.jhipster.padbowl.common.infrastructure.primary.error.PadBowlFieldError.PadBowlFieldErrorBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonDeserialize(builder = PadBowlFieldErrorBuilder.class)
@ApiModel(description = "Error for a field validation")
public class PadBowlFieldError {
  @ApiModelProperty(value = "Path to the field in error", example = "address.country", required = true)
  private final String fieldPath;

  @ApiModelProperty(value = "Technical reason for the invalidation", example = "user.mandatory", required = true)
  private final String reason;

  @ApiModelProperty(value = "Human readable message for the invalidation", example = "Le champ doit être renseigné", required = true)
  private final String message;

  private PadBowlFieldError(PadBowlFieldErrorBuilder builder) {
    fieldPath = builder.fieldPath;
    reason = builder.reason;
    message = builder.message;
  }

  public static PadBowlFieldErrorBuilder builder() {
    return new PadBowlFieldErrorBuilder();
  }

  public String getFieldPath() {
    return fieldPath;
  }

  public String getReason() {
    return reason;
  }

  public String getMessage() {
    return message;
  }

  @JsonPOJOBuilder(withPrefix = "")
  public static class PadBowlFieldErrorBuilder {
    private String fieldPath;
    private String reason;
    private String message;

    public PadBowlFieldErrorBuilder fieldPath(String fieldPath) {
      this.fieldPath = fieldPath;

      return this;
    }

    public PadBowlFieldErrorBuilder reason(String reason) {
      this.reason = reason;

      return this;
    }

    public PadBowlFieldErrorBuilder message(String message) {
      this.message = message;

      return this;
    }

    public PadBowlFieldError build() {
      return new PadBowlFieldError(this);
    }
  }
}
