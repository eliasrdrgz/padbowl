/**
 *
 */
package com.jhipster.padbowl.common.infrastructure.primary.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Collection;
import java.util.List;

@ApiModel(description = "Error result for a WebService call")
public class PadBowlError {
  @ApiModelProperty(value = "Technical type of this error", example = "user.mandatory", required = true)
  private final String errorType;

  @ApiModelProperty(
    value = "Human readable error message",
    example = "Une erreur technique est survenue lors du traitement de votre demande",
    required = true
  )
  private final String message;

  @ApiModelProperty(value = "Invalid fields", required = false)
  private final List<PadBowlFieldError> fieldsErrors;

  public PadBowlError(
    @JsonProperty("errorType") String errorType,
    @JsonProperty("message") String message,
    @JsonProperty("fieldsErrors") List<PadBowlFieldError> fieldsErrors
  ) {
    this.errorType = errorType;
    this.message = message;
    this.fieldsErrors = fieldsErrors;
  }

  public String getErrorType() {
    return errorType;
  }

  public String getMessage() {
    return message;
  }

  public Collection<PadBowlFieldError> getFieldsErrors() {
    return fieldsErrors;
  }
}
