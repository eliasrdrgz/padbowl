package com.jhipster.padbowl.common.infrastructure.primary;

public final class PadBowlApis {
  public static final String GAMES_TAG = "Games";

  private PadBowlApis() {}
}
