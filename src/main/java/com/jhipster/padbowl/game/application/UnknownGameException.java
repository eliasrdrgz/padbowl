package com.jhipster.padbowl.game.application;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;
import com.jhipster.padbowl.game.domain.GameMessage;

class UnknownGameException extends PadBowlException {

  public UnknownGameException() {
    super(
      PadBowlException
        .builder(GameMessage.UNKNOWN_GAME)
        .status(ErrorStatus.NOT_FOUND)
        .message("A user is trying to do an operation on an unknown game")
    );
  }
}
