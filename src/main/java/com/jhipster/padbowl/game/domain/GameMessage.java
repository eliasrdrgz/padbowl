package com.jhipster.padbowl.game.domain;

import com.jhipster.padbowl.common.domain.error.PadBowlMessage;

public enum GameMessage implements PadBowlMessage {
  UNKNOWN_GAME("user.unknown-game"),
  INVALID_ROLL("user.invalid-roll");

  private final String messageKey;

  private GameMessage(String messageKey) {
    this.messageKey = messageKey;
  }

  @Override
  public String getMessageKey() {
    return messageKey;
  }
}
