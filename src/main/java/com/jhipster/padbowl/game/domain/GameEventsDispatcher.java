package com.jhipster.padbowl.game.domain;

import com.jhipster.padbowl.common.domain.Guttered;
import java.util.Collection;

public interface GameEventsDispatcher {
  void dispatch(Collection<Guttered> events);
}
