package com.jhipster.padbowl.game.domain;

import com.jhipster.padbowl.common.domain.Guttered;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.UUID;

public class Game {
  private final List<Integer> rolls = new ArrayList<>();
  private final Deque<Frame> frames = new ArrayDeque<>();

  private final PlayerName player;
  private final GameId id;

  public Game(String playerName) {
    this(null, playerName);
  }

  public Game(UUID id, String playerName) {
    this.id = new GameId(id);
    player = new PlayerName(playerName);
  }

  public GameId getId() {
    return id;
  }

  public PlayerName getPlayer() {
    return player;
  }

  public Collection<Guttered> roll(int pinsDown) {
    rolls.add(pinsDown);

    if (isNewGame()) {
      rollFirst(pinsDown);
    } else {
      rollNotFirst(pinsDown);
    }

    return events(pinsDown);
  }

  private boolean isNewGame() {
    return frames.isEmpty();
  }

  private void rollFirst(int pinsDown) {
    newFrame(pinsDown);
  }

  private void newFrame(int pinsDown) {
    frames.add(new Frame(pinsDown));
  }

  private void rollNotFirst(int pinsDown) {
    Frame current = frames.getLast();
    if (current.isTerminated()) {
      newFrame(pinsDown);
    } else {
      current.secondRoll(pinsDown);
    }
  }

  private List<Guttered> events(int pinsDown) {
    if (pinsDown > 0) {
      return List.of();
    }

    return List.of(new Guttered(this.player));
  }

  public int getScore() {
    int score = 0;
    int frameIndex = 0;

    for (int frame = 0; frame < 10; frame++) {
      score += frameScore(frameIndex);

      frameIndex = nextFrameIndex(frameIndex);
    }

    return score;
  }

  private int frameScore(int frameIndex) {
    if (isStrike(frameIndex)) {
      return 10 + strikeBonus(frameIndex);
    }

    if (isSpare(frameIndex)) {
      return 10 + spareBonus(frameIndex);
    }

    return normalFrameScore(frameIndex);
  }

  private int nextFrameIndex(int frameIndex) {
    if (isStrike(frameIndex)) {
      return frameIndex + 1;
    }

    return frameIndex + 2;
  }

  private boolean isStrike(int frameIndex) {
    return rollScore(frameIndex) == 10;
  }

  private int strikeBonus(int frameIndex) {
    return rollScore(frameIndex + 1) + rollScore(frameIndex + 2);
  }

  private boolean isSpare(int frameIndex) {
    return normalFrameScore(frameIndex) == 10;
  }

  private Integer spareBonus(int frameIndex) {
    return rollScore(frameIndex + 2);
  }

  private int normalFrameScore(int frameIndex) {
    return rollScore(frameIndex) + rollScore(frameIndex + 1);
  }

  private int rollScore(int index) {
    if (index + 1 > rolls.size()) {
      return 0;
    }

    return rolls.get(index);
  }

  public Collection<Frame> getFrames() {
    return Collections.unmodifiableCollection(frames);
  }
}
