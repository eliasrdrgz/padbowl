package com.jhipster.padbowl.game.domain;

import java.util.Optional;

public interface GamesRepository {
  void save(Game game);

  Optional<Game> get(GameId gameId);
}
