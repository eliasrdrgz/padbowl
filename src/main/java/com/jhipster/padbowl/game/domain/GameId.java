package com.jhipster.padbowl.game.domain;

import java.util.UUID;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class GameId {
  private final UUID id;

  public GameId() {
    this(null);
  }

  public GameId(UUID id) {
    this.id = buildId(id);
  }

  private UUID buildId(UUID id) {
    if (id == null) {
      return UUID.randomUUID();
    }

    return id;
  }

  public UUID get() {
    return id;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    GameId other = (GameId) obj;
    return new EqualsBuilder().append(id, other.id).isEquals();
  }
}
