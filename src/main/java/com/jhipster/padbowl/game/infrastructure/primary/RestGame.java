package com.jhipster.padbowl.game.infrastructure.primary;

import com.jhipster.padbowl.game.domain.Game;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@ApiModel(value = "Game")
class RestGame {
  @ApiModelProperty(value = "Unique ID of this game", required = true)
  private final UUID id;

  @ApiModelProperty(value = "Name of the player owning this game", required = true)
  private final String player;

  @ApiModelProperty(value = "Current score of this game", required = true)
  private final int score;

  @ApiModelProperty("Frames for this game")
  private final Collection<RestFrame> frames;

  private RestGame(RestGameBuilder builder) {
    id = builder.id;
    player = builder.player;
    score = builder.score;
    frames = builder.frames;
  }

  public static RestGame from(Game game) {
    return new RestGameBuilder()
      .id(game.getId().get())
      .player(game.getPlayer().get())
      .score(game.getScore())
      .frames(game.getFrames().stream().map(RestFrame::from).collect(Collectors.toList()))
      .build();
  }

  public UUID getId() {
    return id;
  }

  public String getPlayer() {
    return player;
  }

  public int getScore() {
    return score;
  }

  public Collection<RestFrame> getFrames() {
    return frames;
  }

  private static class RestGameBuilder {
    private UUID id;
    private String player;
    private int score;
    private Collection<RestFrame> frames;

    public RestGameBuilder id(UUID id) {
      this.id = id;

      return this;
    }

    public RestGameBuilder player(String player) {
      this.player = player;

      return this;
    }

    public RestGameBuilder score(int score) {
      this.score = score;

      return this;
    }

    public RestGameBuilder frames(Collection<RestFrame> frames) {
      this.frames = frames;

      return this;
    }

    public RestGame build() {
      return new RestGame(this);
    }
  }
}
