package com.jhipster.padbowl.game.infrastructure.primary;

import com.jhipster.padbowl.common.infrastructure.primary.PadBowlApis;
import com.jhipster.padbowl.common.infrastructure.primary.error.PadBowlError;
import com.jhipster.padbowl.game.application.GamesApplicationService;
import com.jhipster.padbowl.game.domain.Game;
import com.jhipster.padbowl.game.domain.GameId;
import com.jhipster.padbowl.security.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/games")
@Api(tags = PadBowlApis.GAMES_TAG)
class GamesResource {
  private final GamesApplicationService games;

  public GamesResource(GamesApplicationService games) {
    this.games = games;
  }

  @PostMapping
  @ApiResponses({ @ApiResponse(code = 500, response = PadBowlError.class, message = "An error occured while creating the game") })
  public ResponseEntity<RestGame> createGame() {
    Game game = games.create(SecurityUtils.getCurrentUserLogin());

    return new ResponseEntity<RestGame>(RestGame.from(game), HttpStatus.CREATED);
  }

  @PostMapping("/{game-id}/rolls")
  @ApiResponses({ @ApiResponse(code = 500, response = PadBowlError.class, message = "An error occured while rolling") })
  public ResponseEntity<RestGame> roll(
    @ApiParam("Id of the game to roll onto") @PathVariable("game-id") UUID id,
    @RequestBody RestRoll roll
  ) {
    Game game = games.roll(new GameId(id), roll.getRoll());

    return ResponseEntity.ok(RestGame.from(game));
  }
}
