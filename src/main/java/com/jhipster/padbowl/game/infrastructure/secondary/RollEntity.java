package com.jhipster.padbowl.game.infrastructure.secondary;

import com.jhipster.padbowl.common.infrastructure.Generated;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Generated
@Table(name = "roll")
@IdClass(RollEntityId.class)
class RollEntity {
  @Id
  @Column(name = "game")
  private UUID game;

  @Id
  @Column(name = "roll")
  private int roll;

  @Column(name = "pins_down")
  private int pinsDown;

  public UUID getGame() {
    return game;
  }

  public void setGame(UUID game) {
    this.game = game;
  }

  public RollEntity game(UUID game) {
    this.game = game;

    return this;
  }

  public int getRoll() {
    return roll;
  }

  public void setRoll(int roll) {
    this.roll = roll;
  }

  public RollEntity roll(int roll) {
    this.roll = roll;

    return this;
  }

  public int getPinsDown() {
    return pinsDown;
  }

  public void setPinsDown(int pinsDown) {
    this.pinsDown = pinsDown;
  }

  public RollEntity pinsDown(int pinsDown) {
    this.pinsDown = pinsDown;

    return this;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(game).append(roll).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    RollEntity other = (RollEntity) obj;
    return new EqualsBuilder().append(game, other.game).append(roll, other.roll).isEquals();
  }
}
