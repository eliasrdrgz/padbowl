package com.jhipster.padbowl.security;

import com.jhipster.padbowl.common.infrastructure.primary.error.NotAuthenticatedUserException;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Utility class for Spring Security.
 */
public final class SecurityUtils {
  private static final String ROLE_PREFIX = "ROLE_";

  private SecurityUtils() {}

  public static String getCurrentUserLogin() {
    return getOptionalUserLogin().orElseThrow(NotAuthenticatedUserException::new);
  }

  public static Optional<String> getOptionalUserLogin() {
    return extractPrincipal(SecurityContextHolder.getContext().getAuthentication());
  }

  public static Optional<String> extractPrincipal(Authentication authentication) {
    if (authentication == null) {
      return Optional.empty();
    }

    Object principal = authentication.getPrincipal();
    if (principal instanceof UserDetails) {
      return Optional.of(((UserDetails) principal).getUsername());
    }

    if (principal instanceof String) {
      return Optional.of((String) principal);
    }

    return Optional.empty();
  }

  public static Set<String> getAuthenticatedUserRoles() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication == null) {
      return Collections.emptySet();
    }

    return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).filter(roles()).collect(Collectors.toSet());
  }

  private static Predicate<String> roles() {
    return authority -> authority.startsWith(ROLE_PREFIX);
  }
}
