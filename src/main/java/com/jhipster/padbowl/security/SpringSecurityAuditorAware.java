package com.jhipster.padbowl.security;

import com.jhipster.padbowl.common.infrastructure.Generated;
import com.jhipster.padbowl.config.Constants;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link AuditorAware} based on Spring Security.
 */
@Generated
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    return Optional.of(SecurityUtils.getOptionalUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
  }
}
