package com.jhipster.padbowl.kipe;

import static com.jhipster.padbowl.kipe.Action.*;
import static com.jhipster.padbowl.kipe.Resource.*;

import com.jhipster.padbowl.common.domain.Username;
import com.jhipster.padbowl.common.infrastructure.primary.error.NotAuthenticatedUserException;
import com.jhipster.padbowl.security.SecurityUtils;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public final class PadBowlAuthorizations {
  private static final Actions DEFAULT_ACTIONS = new Actions();

  private static final Actions NO_ROLE_ACTIONS = new Actions(all("create", GAMES), specific("roll", GAMES));

  private static final Map<String, Actions> ROLES_ACTIONS = Map.of();

  private PadBowlAuthorizations() {}

  public static Username getUsername(Authentication authentication) {
    return SecurityUtils.extractPrincipal(authentication).map(Username::new).orElseThrow(NotAuthenticatedUserException::new);
  }

  public static boolean allAuthorized(Authentication authentication, String action, Resource resource) {
    if (missingAuthorizationInformation(authentication, action, resource)) {
      return false;
    }

    if (NO_ROLE_ACTIONS.allAuthorized(action, resource)) {
      return true;
    }

    return canMakeAction(authentication, canDoAll(action, resource));
  }

  private static Predicate<String> canDoAll(String action, Resource resource) {
    return authority -> userActions(authority).allAuthorized(action, resource);
  }

  public static boolean specificAuthorized(Authentication authentication, String action, Resource resource) {
    if (missingAuthorizationInformation(authentication, action, resource)) {
      return false;
    }

    if (NO_ROLE_ACTIONS.specificAuthorized(action, resource)) {
      return true;
    }

    return canMakeAction(authentication, canDoSpecific(action, resource));
  }

  private static boolean missingAuthorizationInformation(Authentication authentication, String action, Resource resource) {
    return authentication == null || StringUtils.isBlank(action) || resource == null;
  }

  private static boolean canMakeAction(Authentication authentication, Predicate<String> actionChecker) {
    return streamAuthorities(authentication).anyMatch(actionChecker);
  }

  private static Stream<String> streamAuthorities(Authentication authentication) {
    return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority);
  }

  private static Predicate<String> canDoSpecific(String action, Resource resource) {
    return authority -> userActions(authority).specificAuthorized(action, resource);
  }

  private static Actions userActions(String authority) {
    return ROLES_ACTIONS.getOrDefault(authority, DEFAULT_ACTIONS);
  }
}
