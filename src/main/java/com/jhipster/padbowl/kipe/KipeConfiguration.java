package com.jhipster.padbowl.kipe;

import com.jhipster.padbowl.config.SecurityConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@AutoConfigureBefore(SecurityConfiguration.class)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class KipeConfiguration extends GlobalMethodSecurityConfiguration {
  private final CanEvaluator evaluator;

  // All beans here need to be Lazy otherwise it'll break AOP (cache, transactions, etc...)
  public KipeConfiguration(@Lazy CanEvaluator evaluator) {
    this.evaluator = evaluator;
  }

  @Override
  protected MethodSecurityExpressionHandler createExpressionHandler() {
    return new KipeMethodSecurityExpressionHandler(evaluator);
  }
}
