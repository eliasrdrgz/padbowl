package com.jhipster.padbowl.service;

import com.jhipster.padbowl.common.infrastructure.Generated;

@Generated
public class UsernameAlreadyUsedException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public UsernameAlreadyUsedException() {
    super("Login name already used!");
  }
}
