package com.jhipster.padbowl.service;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;
import com.jhipster.padbowl.common.infrastructure.Generated;

@Generated
public class InvalidPasswordException extends PadBowlException {

  public InvalidPasswordException() {
    super(
      PadBowlException.builder(UserMessage.INVALID_PASSWORD).status(ErrorStatus.BAD_REQUEST).message("A user entered an invalid password")
    );
  }
}
